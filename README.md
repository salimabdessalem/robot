# Robot game

### what have been done so far
* create service named movementService that's responsible for the actions of the characters containing all the possible actions
* crate class robot with mappers and DTO in the controller
* create endpoints for the possible actions with the integration tests
* writing unit test for the services
* create a html interface containing a canvas that have a character picture inside with some bottom to control it
* integrate the borders inside the backend instead in the frontend
* writing script to call the endpoints and render the character in each call
### what have been missing
* unit test for the front part (javascript)
* create physical borders in the canvas
* put the border limiter in the front part
* make the web page prettier
* add some background in the interface
* create some security in the backend part using spring security
* add more test to coverage all the possible movement
* upgrade code quality by using patterns like builder and singleton also use interfaces when it comes to services
* if needed I can create submodule to separate the services from the controller
* add more actions for the character like grabbing stuff or eating other objects
## daily report
### First day
* i created the robot class as a model i make it static in order to save data, i chose the X = 0 , Y=5 and position= DOWN in order for the robot to be in the top left corner facing down,
  <br> also i created a service class that responsible in different moves as forward, turn around and turning right...
* tomorrow I will work on the mappers and create the controllers and create some integration tests and unit tests for my classes.

### Second day
* i finished creating the endpoints in controller layer given each action a specific url and send the robot position via the response by writing the dto and mappers.
* start create integration tests for the controller and for the service.
* tomorrow i will start working on the front part and if i had time i will put some grid limitation in the backend parts with its tests

### Third day
* fix problem that accrue to me when i wanted to send a post request to backend by adding cors configs in the backend
* start working on the front by creating the canvas and inserting the image , also create the forward, and turning around  functions by testing if  the picture if moving and turning
* improve some code and fix some bugs

### Forth day
* improve front and back code, fix some bugs
* add some new function in the front end and improve the previous ones by adding more conditions and clean the code
* make the character move on 5 tiles only instead move in unlimited pixels
* adding exceptions in the backend when the character pass the fifth tile

###fifth day
* fix the integration test implementation and create more integration tests
*  create unit test from movementService 
