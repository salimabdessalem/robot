package com.personal.robot.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(OutOfBoundException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public void handleException() {
    }
}
