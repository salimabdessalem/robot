package com.personal.robot.exception;

public class OutOfBoundException  extends RuntimeException{

    private static final long serialVersionUID = 2L;

    public OutOfBoundException(String message) {
        super(message);
    }

    public OutOfBoundException(Throwable cause) {
        super(cause);
    }

    public OutOfBoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
