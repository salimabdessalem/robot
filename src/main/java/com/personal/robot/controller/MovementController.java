package com.personal.robot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.personal.robot.dto.RobotDTO;
import com.personal.robot.mapper.RobotMapper;
import com.personal.robot.service.MovementService;


/**
 * @author s.abdessalem on 8/4/2021
 */
@RestController
@RequestMapping("/api")
public class MovementController {

	private final MovementService movementService;

	private final RobotMapper robotMapper;

	@Autowired
	public MovementController(MovementService movementService, RobotMapper robotMapper) {
		this.movementService = movementService;
		this.robotMapper = robotMapper;
	}

	@PostMapping("/forward/{id}")
	public RobotDTO forward(@PathVariable("id") int id) {
		switch (id) {
			case 1:
				movementService.move(1);
				break;
			case 2:
				movementService.move(2);
				break;
			case 3:
				movementService.move(3);
				break;
		}
		return robotMapper.toDTO();
	}

	@PostMapping("/right")
	public RobotDTO right() {
		movementService.right();
		return robotMapper.toDTO();
	}

	@PostMapping("/initial")
	public RobotDTO initial() {
		movementService.initial();
		return robotMapper.toDTO();
	}


	@PostMapping("/turnaround")
	public RobotDTO turnAround() {
		movementService.turnAround();
		return robotMapper.toDTO();
	}

	@PostMapping("/wait")
	public RobotDTO robotWait() {
		return robotMapper.toDTO();
	}


}
