package com.personal.robot.service;

import com.personal.robot.exception.OutOfBoundException;
import com.personal.robot.model.Position;
import com.personal.robot.model.Robot;
import org.springframework.stereotype.Service;


/**
 * @author s.abdessalem on 8/4/2021
 */
@Service
public class MovementService {

    public void initial() {
        Robot.setPositionX(0);
        Robot.setPositionY(0);
        Robot.setPosition(Position.DOWN);
    }

    public void move(int steps) {
        switch (Robot.getPosition()) {
            case UP:
                if (Robot.getPositionY() - steps >= 0) {
                    Robot.setPositionY(Robot.getPositionY() - steps);
                    break;
                } else {
                    throw new OutOfBoundException("the character is out of tiles");
                }

            case RIGHT:
                if (Robot.getPositionX() + steps <= 5) {
                    Robot.setPositionX(Robot.getPositionX() + steps);
                    break;
                } else {
                    throw new OutOfBoundException("the character is out of tiles");
                }
            case LEFT:
                if (Robot.getPositionX() - steps >= 0) {
                    Robot.setPositionX(Robot.getPositionX() - steps);
                    break;
                } else {
                    throw new OutOfBoundException("the character is out of tiles");
                }
            case DOWN:
                if (Robot.getPositionY() + steps <= 5) {
                    Robot.setPositionY(Robot.getPositionY() + steps);
                    break;
                } else {
                    throw new OutOfBoundException("the character is out of tiles");
                }
        }
    }

    public void turnAround() {
        switch (Robot.getPosition()) {
            case UP:
                Robot.setPosition(Position.DOWN);
                break;
            case RIGHT:
                Robot.setPosition(Position.LEFT);
                break;
            case LEFT:
                Robot.setPosition(Position.RIGHT);
                break;
            case DOWN:
                Robot.setPosition(Position.UP);
                break;
        }
    }

    public void right() {
        switch (Robot.getPosition()) {
            case UP:
                Robot.setPosition(Position.RIGHT);
                break;
            case RIGHT:
                Robot.setPosition(Position.DOWN);
                break;
            case LEFT:
                Robot.setPosition(Position.UP);
                break;
            case DOWN:
                Robot.setPosition(Position.LEFT);
                break;
        }
    }
}
