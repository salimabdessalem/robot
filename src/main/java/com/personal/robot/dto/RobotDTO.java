package com.personal.robot.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.personal.robot.model.Position;


/**
 * @author s.abdessalem on 8/4/2021
 */
@JsonInclude(value = Include.NON_NULL)
@JsonDeserialize
public class RobotDTO {

	private int positionX;

	private int positionY;

	private Position position;

	public int getPositionX() {
		return positionX;
	}

	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}
}
