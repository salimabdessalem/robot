package com.personal.robot.model;

/**
 * @author s.abdessalem on 8/4/2021
 */
public class Robot {
	private static int positionX = 0;

	private static int positionY = 5;

	private static Position position = Position.DOWN;

	public static int getPositionX() {
		return positionX;
	}

	public static void setPositionX(int positionX) {
		Robot.positionX = positionX;
	}

	public static int getPositionY() {
		return positionY;
	}

	public static void setPositionY(int positionY) {
		Robot.positionY = positionY;
	}

	public static Position getPosition() {
		return position;
	}

	public static void setPosition(Position position) {
		Robot.position = position;
	}
}
