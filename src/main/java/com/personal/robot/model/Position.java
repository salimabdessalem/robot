package com.personal.robot.model;

/**
 * @author s.abdessalem on 8/4/2021
 */
public enum Position {
	RIGHT, LEFT, UP, DOWN
}
