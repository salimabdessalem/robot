package com.personal.robot.mapper;

import org.springframework.stereotype.Component;

import com.personal.robot.dto.RobotDTO;
import com.personal.robot.model.Robot;


/**
 * @author s.abdessalem on 8/4/2021
 */
@Component
public class RobotMapper {

	public RobotDTO toDTO() {
		RobotDTO robotDTO = new RobotDTO();
		robotDTO.setPosition(Robot.getPosition());
		robotDTO.setPositionX(Robot.getPositionX());
		robotDTO.setPositionY(Robot.getPositionY());
		return robotDTO;
	}
}
