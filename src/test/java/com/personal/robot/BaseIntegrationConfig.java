package com.personal.robot;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.validation.ValidatorFactory;


/**
 * @author s.abdessalem on 8/6/2021
 */
@Configuration
@EnableWebMvc
public class BaseIntegrationConfig implements WebMvcConfigurer {

	@Bean
	public ObjectMapper objectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.findAndRegisterModules();
		return mapper;
	}

	@Bean
	public MethodValidationPostProcessor methodValidationPostProcessor(WebApplicationContext webApplicationContext) {
		MethodValidationPostProcessor proc = new MethodValidationPostProcessor();
		proc.setValidatorFactory(validatorFactory(webApplicationContext));
		return proc;
	}

	private ValidatorFactory validatorFactory(WebApplicationContext webApplicationContext) {
		LocalValidatorFactoryBean factory = new LocalValidatorFactoryBean();
		factory.setApplicationContext(webApplicationContext);
		factory.afterPropertiesSet();

		return factory;
	}
}
