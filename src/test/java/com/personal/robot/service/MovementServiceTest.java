package com.personal.robot.service;

import com.personal.robot.exception.OutOfBoundException;
import com.personal.robot.model.Position;
import com.personal.robot.model.Robot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@DisplayName("movementService Test")
public class MovementServiceTest {

    @InjectMocks
    MovementService movementService;

    @BeforeEach
    void setup() {
        Robot.setPosition(Position.DOWN);
        Robot.setPositionX(0);
        Robot.setPositionY(0);
    }

    @ParameterizedTest
    @ValueSource(ints = {1,2,3})
    @DisplayName("move method")
    void test_move_method(int steps) {
        movementService.move(steps);
        assertEquals(Robot.getPositionY(), steps);
        assertEquals(Robot.getPositionX(), 0);
        assertEquals(Robot.getPosition(), Position.DOWN);
    }

    @Test
    @DisplayName("turnaround method")
    void test_turnaround() {
        movementService.turnAround();
        assertEquals(Robot.getPositionY(), 0);
        assertEquals(Robot.getPositionX(), 0);
        assertEquals(Robot.getPosition(), Position.UP);
    }

    @Test
    @DisplayName("right method")
    void test_right() {
        movementService.right();
        assertEquals(Robot.getPositionY(), 0);
        assertEquals(Robot.getPositionX(), 0);
        assertEquals(Robot.getPosition(), Position.LEFT);
    }

    @ParameterizedTest
    @ValueSource(ints = {1,2,3})
    @DisplayName("move method")
    void test_move_method_with_exception(int steps) {
        Robot.setPosition(Position.DOWN);
        Robot.setPositionX(0);
        Robot.setPositionY(5);
        assertThrows(OutOfBoundException.class, () -> movementService.move(steps));
        assertNotEquals(Robot.getPositionY(), steps);
        assertEquals(Robot.getPositionX(), 0);
        assertEquals(Robot.getPosition(), Position.DOWN);
    }
}
