package com.personal.robot;

import org.junit.jupiter.api.Test;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
@ConfigurationPropertiesScan
class RobotApplicationTests {

	@Test
	void contextLoads() {
	}

}
