package com.personal.robot.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.personal.robot.dto.RobotDTO;
import com.personal.robot.model.Position;
import com.personal.robot.model.Robot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * @author s.abdessalem on 8/5/2021
 */
@SpringBootTest
@AutoConfigureMockMvc
public class MovementControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    private static final String BASE_URL = "/api";

    @BeforeEach
    void setup() {
        Robot.setPosition(Position.DOWN);
        Robot.setPositionX(0);
        Robot.setPositionY(0);
    }

    @Test
    @DisplayName("test wait endpoint")
    void test_wait() throws Exception {
        RobotDTO robot = new RobotDTO();
        robot.setPosition(Position.DOWN);
        robot.setPositionX(0);
        robot.setPositionY(0);
        final String expectedResults = objectToJson(robot);
        mockMvc.perform(post(BASE_URL + "/wait").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResults, true));
    }

    protected String objectToJson(Object object) throws JsonProcessingException {
        return objectMapper.writeValueAsString(object);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3})
    @DisplayName("test move forward return 200")
    void test_forward_one_step_return_200(int steps) throws Exception {
        RobotDTO robot = new RobotDTO();
        robot.setPosition(Position.DOWN);
        robot.setPositionX(0);
        robot.setPositionY(steps);
        final String expectedResults = objectToJson(robot);
        mockMvc.perform(post(BASE_URL + "/forward/"+steps).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResults, true));
    }

    @Test
    @DisplayName("test right endpoint")
    void test_right() throws Exception {
        RobotDTO robot = new RobotDTO();
        robot.setPosition(Position.LEFT);
        robot.setPositionX(0);
        robot.setPositionY(0);
        final String expectedResults = objectToJson(robot);
        mockMvc.perform(post(BASE_URL + "/right").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResults, true));
    }

    @Test
    @DisplayName("test turnaround endpoint")
    void test_turnaround() throws Exception {
        RobotDTO robot = new RobotDTO();
        robot.setPosition(Position.UP);
        robot.setPositionX(0);
        robot.setPositionY(0);
        final String expectedResults = objectToJson(robot);
        mockMvc.perform(post(BASE_URL + "/turnaround").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResults, true));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3})
    @DisplayName("test move forward return 200")
    void test_forward_one_step_return_403(int steps) throws Exception {
        Robot.setPosition(Position.DOWN);
        Robot.setPositionX(0);
        Robot.setPositionY(5);
        RobotDTO robot = new RobotDTO();
        robot.setPosition(Position.DOWN);
        robot.setPositionX(0);
        robot.setPositionY(5+steps);
        final String expectedResults = objectToJson(robot);
        mockMvc.perform(post(BASE_URL + "/forward/"+steps).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("test initial endpoint")
    void test_initial() throws Exception {
        Robot.setPosition(Position.LEFT);
        Robot.setPositionX(3);
        Robot.setPositionY(4);
        RobotDTO robot = new RobotDTO();
        robot.setPosition(Position.DOWN);
        robot.setPositionX(0);
        robot.setPositionY(0);
        final String expectedResults = objectToJson(robot);
        mockMvc.perform(post(BASE_URL + "/initial").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResults, true));
    }


}
